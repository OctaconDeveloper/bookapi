<?php

namespace App;
use App\Book;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
	protected $hidden = array('created_at', 'updated_at','pivot');
    function book(){
     	return $this->belongsTo(Book::class);
     }


    public static function getBookID(){
   		return DB::table('reviews')->select('book_id')
    	->groupBy('book_id')
	    ->orderByRaw('COUNT(*) DESC')
	    ->get();
	}
}
