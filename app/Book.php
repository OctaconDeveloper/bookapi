<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Author;
use App\Review;
class Book extends Model
{
	 protected $hidden = array('created_at', 'updated_at','pivot');

   public function authors(){

     	return $this->belongsToMany(Author::class);

     }

	
     function reviews(){
     	return $this->hasMany(Review::class);
     }
    
    public function scopeGetAvg($review, $book)
{
    return $review->where('book_id', $book->id);
}
}
