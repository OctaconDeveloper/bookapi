<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class AuthorBook extends Model
{
    protected $hidden = ['pivot'];
    protected $fillable = ['author_id','book_id'];

   public static function getBookID($author){
   	return  DB::table('author_book')->select('book_id')->whereIn('author_id',$author)->get();
   }

 
}
