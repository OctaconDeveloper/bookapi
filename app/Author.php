<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Book;

class Author extends Model
{
	 protected $hidden = array('created_at', 'updated_at','pivot');
	 
    function books(){

     	return $this->belongsToMany(Book::class);

     }
}
