<?php

namespace App\Http\Controllers;


use App\Book;
use App\Review;
use App\Author;
use App\AuthorBook;

use Illuminate\Support\Facades\DB;
use App\Http\Resources\BookResource;
use App\Http\Resources\BookResourceCollection;
use Illuminate\Http\Request;

class BookController extends Controller
{
	
    protected function  title_sort($title){
     	return Book::orderBy($title)->paginate('10');
    }

/****
INDEX
******/
    public function index(Request $request) 
    {
    	
    	if($request->sortDirection){
    		$sortArray = ['ASC','DESC'];
    		if(in_array($request->sortDirection, $sortArray)){
    			$data = Book::orderBy('id',$request->sortDirection)->paginate('5');
    			return new BookResourceCollection($data);
    		}else{
        		$status = 422;
    			$response = [
            		'status' => $status,
            		'errors' => 'Invalid Sort Parameter.'
        		];
    			return response()->json($response, $status);
    		}
    	}else
    	if($request->sortColumn){
    		if($request->sortColumn=='title'){
    			return $this->title_sort($request->sortColumn);
    		}else if($request->sortColumn=='avg_review'){
    			 $autholog = Review::getBookID();
    			foreach($autholog as $value)
	    			$array[] = $value->book_id;
	    		return $allLog =  new BookResourceCollection(Book::whereIn('id',$array)->get());
    		}else{
    			$status = 422;
    			$response = [
            		'status' => $status,
            		'errors' => 'Invalid Sort Parameter.'
        		];
    			return response()->json($response, $status);
    		}
    		
    	}else
    	if($request->authors){
    		 $authors = explode(',', $request->authors);
    		if(Author::whereIn('id',$authors)->count() > 0){
	    		$autholog = AuthorBook::getBookID($authors);
	    		foreach($autholog as $value)
	    			$array[] = $value->book_id;
	    		return $allLog =  new BookResourceCollection(Book::whereIn('id',$array)->get());
	    	}else{
	    		$status = 422;
    			$response = [
            		'status' => $status,
            		'errors' => 'Invalid Sort Parameter.'
        		];
    			return response()->json($response, $status);
	    	}

    	}else if($request->title){
    		 
    		if(Book::where('title',$request->title)->count() > 0){
    			 $data = Book::where('title',$request->title)->first();
	    		 return new BookResource($data);
	    	}else{
	    		$status = 422;
    			$response = [
            		'status' => $status,
            		'errors' => 'No Record of Title Found'
        		];
    			return response()->json($response, $status);
	    	}
	    }else{
    		$data = Book::paginate('5');
    		return new BookResourceCollection($data);    	
    	}
    }







/****
STORE
******/
     public function store(Request $request) 
    {
    		if(Book::where('isbn',$request->isbn)->count() > 0){
    			$status = 422;
    			$response = [
            		'status' => $status,
            		'errors' => 'ISBN already Captured'
        		];
        		return response()->json($response, $status);
    		}else{
    			$data = array(
    				'isbn' => $request->isbn,
    				'title' => $request->title,
    				'description' => $request->description,
    			);
	    		$book = DB::table('books')->insertGetId($data);

	    		$authors = explode(',', $request->authors);
	    		foreach($authors as $author){
	    			$data2 = array(
	    				'author_id' => $author,
	    				'book_id' => $book,
	    			);
	    			$autor = DB::table('author_book')->insertGetId($data2);
	    		};    				
    				return new BookResource(Book::find($book));
    	} 

    	//return new BookResource(Book::find($book->id)); 
    }
}
