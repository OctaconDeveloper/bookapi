<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
class TokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!User::where('id',$request->id)->where('remember_token',$request->token)->count() > 0){
            return response()->json(['error'=>'Invalid Token','status'=>422]);
        }
        return $next($request);
    }
}
