<?php

namespace App\Http\Middleware;

use Closure;
use App\RoleUser;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!RoleUser::where('user_id',$request->id)->where('role_id','1')->count() > 0){
            return response()->json(['error'=>'Unathorized User','status'=>422]);
        }
        return $next($request);
    }
}
