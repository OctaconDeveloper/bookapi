<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
         return [
                'id' => $this->id,
                'title' => $this->title,
                'isbn' => $this->isbn,
                'description' => $this->description,
                'authors' => $this->authors,
                'reviews' => [
                    'avg' => $this->getAverage($this->reviews()),
                    'count' => $this->countReviews($this->reviews()),
                ]
             ];
    }

    protected function countReviews($reviews)
    {
        $count = $reviews->pluck('id')->count();
        return $count;
    }

    protected function getAverage($reviews)
    {
        $avg = $reviews->pluck('review')->avg();
        return $avg;
    }
}
