<?php

use Illuminate\Database\Seeder;
use App\AuthorBook;
class AuthorBookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AuthorBook::class, 15)->create();
    }
}
