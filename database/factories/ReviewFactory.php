<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Review;
use App\User;
use App\Book;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {
    return [
    	'user_id'=> User::all()->random()->id,
    	'book_id'=> Book::all()->random()->id,
       	'review' =>$faker->numberBetween($min = 1, $max = 10),
        'comment' =>$faker->sentence($nbWords = 6, $variableNbWords = true)
       ];
});
