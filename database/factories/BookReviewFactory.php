<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BookReview;
use App\Book;
use App\Review;
use Faker\Generator as Faker;

$factory->define(BookReview::class, function (Faker $faker) {
    return [
        'book_id' => Book::all()->random()->id,
        'review_id' => Review::all()->random()->id,
    ];
});
