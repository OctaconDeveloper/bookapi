<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'title' =>$faker->unique()->sentence($nbWords = 6, $variableNbWords = true),
        'isbn' =>$faker->unique()->randomNumber(9),
        'description' =>$faker->realText($maxNbChars = 200, $indexSize = 2)
    ];
});